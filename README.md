# utils

This repository will contains all my utils scripts. 

Web : 
- winit
- flask-init (WIP)

__Flask-init :__
The flask init commnad initialize basic structure for flask project.
Creates :
   - database
   - templates
       - layouts
       - pages
       - partials
   - static
       - css
       - scripts
       - assets
- main.py
- requirements.txt
- .gitignore

FIXME : 
- fix undersirable argument
    - Works even when argument is not in argument list