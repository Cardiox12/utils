#!/bin/bash

# Flask-init creates basic structure for beginning a flask project.
# DO : 
# creating git directory
# creating folders : 
#   - database
#   - templates
#       - layouts
#       - pages
#       - partials
#   - static
#       - css
#       - scripts
#       - assets
# creating files : 
#   - main.py
#   - requirements.txt
#   - .gitignore

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

files=(main.py requirements.txt .gitignore)
folders=(templates/{layouts,pages,partials} static/{css,scripts,assets} database)
add_folders=(templates/pages static/css static/scripts)
add_extensions=(html css js)
version="0.0.2"
usage="$(basename "$0") [-h | --help] [--env] [-v | --version] [add page_name] -- program to init basic structure for flask app
\n
where: \n
\t    -h --help     \t show help \n
\t    --env         \t init a virtualenv for the flask app \n
\t    add           \t add a new vue to flask app, it basically add html and static file to templates \n
\t    -v --version  \t show current version of flask-init \n
"

if [ "$1" = "--help" ]||[ "$1" = "-h" ]; then
    printf  "$usage"
    exit
fi

if [ "$1" = "--env" ]; then
    if [ "$(command -v python3)" ]&&[ "$(command -v virtualenv)" ]; then
        virtualenv -p python3 env
    else
        printf "${RED}You need to install python or virtualenv" 
    fi
fi

if [ "$1" = "-v" ]||[ "$1" = "--version" ]; then
    printf  "Flask-init \nVersion : $version"
    exit
fi

if [ "$1" = "add" ]; then
    if [ ! -z "$2" ]; then
        page=$2
        for (( i=0 ; i < ${#add_folders[@]} ; ++i )); do
            add_page="${add_folders[i]}/$page.${add_extensions[i]}"
            if [ ! -d "${add_folders[i]}" ]; then
                printf  "${RED}Folders does not exists, please run \n\n\t${GREEN}flask-init\n\n"
                exit
            fi
            if [ -e "$add_page" ]; then
                printf "${RED}$add_page already exists\n"
            else
                printf "Creating ${GREEN}$add_page\n"
                touch "$add_page"
            fi
        done
    else
        printf  "$usage"
    fi
    exit
fi

if [ ! -d ".git" ]; then
    git init 
fi

for folder in "${folders[@]}"; do
    if [ ! -d "$folder" ]; then
        mkdir -p "$folder"
        printf "${NC}Creating ${GREEN}$folder\n"
    fi
done

for file in "${files[@]}"; do
    if [ ! -e "$file" ]; then
        touch "$file"
        printf "${NC}Creating ${GREEN}$file\n"
    fi
done

if [ -e "${files[1]}" ]; then 
    printf "flask" > ${files[1]}
fi
